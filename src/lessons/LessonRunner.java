package lessons;

public class LessonRunner
{
    public static void run(Lesson lesson, Object ...args)
    {
        lesson.start(args);
    }
}
