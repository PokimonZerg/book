package lessons;

public interface Lesson
{
    void start(Object ...args);
}
