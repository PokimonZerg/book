package common;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

public class ConsoleController implements Initializable
{
    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        textArea.setEditable(false);
    }
    
    @FXML
    private TextArea textArea;
    @FXML
    private TextField textField;
}
