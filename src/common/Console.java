package common;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Консоль на JavaFX
 * Реализцет базовые функции консоли
 */
public class Console
{
    public Console(Stage stage, String title) throws Exception
    {
        createConsole(stage, title);
    }
    
    public Console(Stage stage) throws Exception
    {
        createConsole(stage, "Чудесная консоль");
    }
    
    private void createConsole(Stage stage, String title) throws Exception
    {
        // меняем заголовок консоли
        stage.setTitle(title);
        // загружаем fxml прямо из ярника
        stage.setScene(new Scene((Parent)FXMLLoader.load(getClass().getResource("Console.fxml"))));
        // меняем иконку
        stage.getIcons().add(new Image(getClass().getResourceAsStream("icon.png")));
        // делаем окно нерастягиваемым
        stage.setResizable(false);
        // показываем окно
        stage.show();
    }
}
